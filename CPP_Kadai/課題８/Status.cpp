#include "Status.h"

bool Status::SetLv(int i)
{
	if (i <= 0)
	{
	    return false;
	}

	if (i >= 100)
	{
		i = 99;
	}

	lv = i;
	return true;
}

void Status::Calc()
{
	hp = lv * lv + 50;
	atk = lv * 10;
	def = lv * 9;
}

int Status::GetHp()
{
	return hp;
}

//攻撃力値を返す（アクセス関数）
int Status::GetAtk()
{
	return atk;
}

//防御力値を返す（アクセス関数）
int Status::GetDef()
{
	return def;
}
