#include < iostream>
#include "Data.h"

int main()
{
	int i;
	std::cin >> i;

	//Dataクラスのインスタンスを作る
	Data x;

	x.SetValue(i);  //変数iの値を渡す（アクセス関数）
	x.Disp();       //表示する
}