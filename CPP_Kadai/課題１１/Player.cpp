#include "Player.h"
#include <iostream>

Player::Player(void)
{
	hp = 300;
	atk = 50;
	def = 35;
}

void Player::DispHp()
{
	std::cout << "プレイヤーHP＝" << hp << "\n";

}

int Player::Attack(int i)
{
	std::cout << "プレイヤーの攻撃！" << "\n";
	return (atk - i) / 2;
}

void Player::Damage(int i)
{
	std::cout << "プレイヤーは" << i << "のダメージ\n";
	hp - i;
}

int Player::GetDef()
{
	return def;
}

bool Player::IsDead()
{
	if (hp < 0)
	{
		return true;
	}
	return false;
}
